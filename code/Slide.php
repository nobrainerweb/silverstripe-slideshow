<?php

class Slide extends DataObject {
	
	private static $db = array(
		'Title' => 'Text',
		'Text' => 'Text',
		'ExternalLink' => 'Varchar(255)',
		'Active' => 'Boolean(1)',
		"SortOrder" => "Int"
	);
	private static $has_one = array(
		'Image' => 'Image',
		'InternalLink' => 'SiteTree',
		'Page' => 'Page'
	);

	private static $default_sort = 'SortOrder';
	
	private static $defaults = array(	
		'Active' => 1
	);
	
	public function getIsActive(){
		return $this->Active ? 'Yes' : 'No';
	}
	
	static $field_labels = array();
	
	static $summary_fields = array(
		'Thumbnail' => 'Thumbnail',
		'Title' => 'Title',
		'getIsActive' => 'Active?'
	);	
		
	
	public function getCMSFields(){
		$fields = parent::getCMSFields();
		
		$fields->removeFieldsFromTab('Root.Main', array('InternalLinkID','ExternalLink','PageID','SortOrder','Title','Text', 'Image'));
		
		$fields->addFieldsToTab("Root.Main", TextField::create('Title'));

		$fields->addFieldsToTab("Root.Main", TextareaField::create('Text')->setRows(3));

		$internallink = DisplayLogicWrapper::create(TreeDropdownField::create("InternalLinkID", "Choose a page", "SiteTree"));
		$externallink = TextField::create("ExternalLink", "Link to external page");

		$fields->addFieldsToTab("Root.Main", array(
			OptionsetField::create("LinkType", "", array('internal-link' => 'Link to an internal page', 'external' => 'Link to an external page')),
			$internallink,
			$externallink
		));
		$externallink
			->displayIf("LinkType")->isEqualTo("external")
			->end();
		$internallink
			->displayIf("LinkType")->isEqualTo("internal-link")
			->end();
		 
		$uploadField = UploadField::create('Image','Image');
		$uploadField->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
		$uploadField->setFolderName('Slideshow');
		$fields->addFieldsToTab("Root.Main", $uploadField);

		//allow extending this object with another 
		$this->extend('updateCMSFields', $fields);
		
		return $fields;
	}

	public function getThumbnail() {
		if (((int) $this->ImageID > 0) && (is_a($this->Image(),'Image')))  {
	   return $this->Image()->SetWidth(50); 
		} else {
			return '(No image)';
		}
	}
	
}
