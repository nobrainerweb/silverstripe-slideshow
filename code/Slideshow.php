<?php

class Slideshow extends DataExtension {
	
	private static $has_many = array(
		'Slides' => 'Slide'
	);

	public function updateCMSFields(FieldList $fields) {
		
		$config = GridFieldConfig_RecordEditor::create(10);
		$config->addComponent(new GridFieldOrderableRows('SortOrder'));
		$gridField = new GridField('Slides', "Slides", $this->owner->Slides(), $config);
		$fields->addFieldToTab('Root.Slideshow', $gridField);
		return $fields;
	}
	
	public function ListSlides($num = 10) {
		$slides = Slide::get()->filter(array(
			'PageID' => $this->owner->ID,
		    'Active' => '1'
		))->sort('SortOrder', 'DESC');
		
		return $slides;
	}
	
}